
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Conference - Responsive HTML5 Template</title>

    <!-- favicon -->
    <link href="favicon.png" rel=icon>

    <!-- web-fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,500' rel='stylesheet' type='text/css'>
    <link href='css/hjcss.css' rel='stylesheet' type='text/css'>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar">
<div id="main-wrapper">
    <header class="header">
        <!-- Navigation -->
        <nav class="navbar main-menu" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target=".navbar-main-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand page-scroll" href="#page-top"><img src="img/smartcity6.png" alt=""></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                    <ul class="nav navbar-nav">
                        <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                        <li class="hidden"><a href="#page-top"></a></li>
                        <li><a class="page-scroll" href="#section-venue">محل برگزاری</a></li>
                        <li><a class="page-scroll" href="#section-partner">حامیان</a></li>
                        <li><a class="page-scroll" href="#section-pricing">ثبت نام</a></li>
                        <li><a class="page-scroll" href="#section-ajenda">تقویم برگزاری</a></li>
                        <li><a class="page-scroll" href="#section-speaker">سخنرانان</a></li>
                        <li><a class="page-scroll" href="#section-intro">محور های همایش</a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>

        <!-- .nav -->

    </header>
    <div class="jumbotron text-center">
        <div class="content">

            <div class="container" style="margin-top:30px;max-height: 200px;max-width: 500px;direction:rtl">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"> ورود به پنل کاربری</h3>
                    </div>
                    <div class="panel-body" >
                        <form role="form" action="user.php" method="get">
                            <fieldset>
                                <div class="form-group" >
                                    <input class="form-control" placeholder="پست الکترونیکی شما" name="email" type="email"
                                           autofocus="">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="رمز ورود" name="password" type="password"
                                           value="">
                                </div>
                                <div class="checkbox" style="direction: ltr">
                                    <label class="checkbox-inline">
                                        <input  name="remember" type="checkbox" style="margin-top: 8px" >
                                        <h style="color: #000"> من را به خاطر بسپار</h>
                                    </label>
                                </div>

                                <input type="hidden" name="type" value="reg" />
                                <input type="submit" value="Submit">
                                <!-- Change this to a button or input when using this as a form -->
                                <a type="submit" class="btn btn-sm btn-success">ورود</a>
                                <a href="show();" class="btn btn-sm btn-primary">ثبت نام</a>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script language="javascript">
function show()
{
    alert("HelloWorld");
}
</script>



</html>