<?php
//if there is not a session start a session
require_once __DIR__.'/../db.php';
if(!isset($_SESSION))
{
    session_start();
}
/**
 * Created by PhpStorm.
 * User: Hamed
 * Date: 6/25/2017
 * Time: 6:42 PM
 */
//if there is a command that posted
if (isset($_POST['command'])) {
    //and it is "updateuserinfo"
    if ($_POST['command'] == "updateuserinfo") {
        //update user info by passing email,password,name,family,gender,degree
        UpdateUserInfo($_POST['email'], $_POST['password'], $_POST['name'], $_POST['family'], $_POST['gender'], $_POST['degree']);
    } //and if it is "addwriter"
    else if ($_POST['command'] == "addwriter") {
        //addWriterToArticle by passing article_id,writer_name,writer_family,writer_email,writer_degree,writer_affiliation
        addWriterToArticle($_POST['article_id'], $_POST['writer_name'], $_POST['writer_family'], $_POST['writer_email'], $_POST['writer_degree'], $_POST['writer_affiliation']);
    } //and if it is "retArticlesForExecutive"
    else if ($_POST['command'] == "retArticlesForExcutive") {
        retArticlesForExcutive($_SESSION['login_user'], $_POST['type']);
    }
    else if ($_POST['command'] == "retArticlesForJudge") {
        retArticlesForJudge($_SESSION['login_user'], $_POST['type']);
    }
    else if($_POST['command'] == "getJudges")
    {
        getJudges();
    }
    else if($_POST['command'] == "retArticlesForManager")
    {
        retArticlesForManager($_POST['type']);
    }
    else if($_POST['command'] == "SubmitArticleToExecutive")
    {
        admin_submitArticleToExecutive();
    }
    else if($_POST['command'] == "promoteuser")
    {
        admin_UpdateUserTitle();
    }
    else if($_POST['command'] == "submittojudges")
    {
        executive_submitArticleToJudges();
    }
    else if($_POST['command'] == "AcceptArticle")
    {
        AcceptArticle();
    }
    else if($_POST['command'] == "RejectArticle")
    {
        RejectArticle();
    }
}
function dberror()
{
    global $connection;
    //will alert warning errors
    echo '
                <div class="alert">
                  <span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span> 
                  ' . mysqli_error($connection) . '
                </div>
        ';
}
function returnPageofHere($page)
{
    return "panel.php" . "?p=" . $page;
}
function getUserValue($field)
{
    global $connection;
    if (isset($_SESSION['login_user'])) {
        $query = "SELECT $field FROM users WHERE users.email ='" . $_SESSION['login_user'] . "'";
        $res = mysqli_query($connection, $query);
        if (mysqli_num_rows($res) == 1) {
            mysqli_data_seek($res, 0);
            return mysqli_fetch_row($res)[0];
        }
        //TODO @sina to @hamed check this pls and if it good to go,uncomment it
//        else{
//            dberror();
//        }
    }
}
function getArticleFields($article_id)
{
    global $connection;
    $query = "SELECT title,keywords,abstract_fa,abstract_en,url,status from articles where ID = '$article_id'" ;
    $res = mysqli_query($connection, $query);
    if($res == false)
    {
        dberror();
    }
    else
    {
        mysqli_data_seek($res, 0);
        return mysqli_fetch_row($res);
    }

}
function UpdateUserInfo($email, $password, $name, $family, $gender, $degree)
{
    global $connection;
    //give these items and check if there is answer to this query or not
    $query = "UPDATE USERS ";
    $query .= "set password = '$password',";
    $query .= "name = '$name',";
    $query .= "family = '$family',";
    $query .= "gender = '$gender',";
    $query .= "degree = '$degree' ";
    $query .= "where email = '$email' ";
    $res = mysqli_query($connection, $query);
    //if result is false
    if ($res == false) {
        //show alert that display errors
        echo '
                <div class="alert">
                  <span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span> 
                  ' . mysqli_error($connection) . '
                </div>
        ';
    } //and if there is
    else {
        //show alert that notify that table was updated successful
        echo '
                <div class="info">
                  <span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span> 
                  اطلاعات با موفقیت به روز رسانی شد
                </div>
        ';
    }
}
//addWriterToArticle will give this values
function addWriterToArticle($article_id, $writername, $writerfamily, $writeremail, $writerdegree, $writeraffiliation)
{
    global $connection;
    $query = "INSERT INTO `writers` (`name`,`family`,`email`,`degree`,`affiliation`)";
    $query .= "VALUES (";
    $query .= "'$writername',";
    $query .= "'$writerfamily',";
    $query .= "'$writeremail',";
    $query .= "'$writerdegree',";
    $query .= "'$writeraffiliation'";
    $query .= ")";
    $res = mysqli_query($connection, $query);
    if ($res == false) {
        //this show error why won't be execute
        echo '
                <div class="alert">
                  <span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span> 
                  ' . mysqli_error($connection) . '
                </div>
        ';
        //this will be execute if writers is successfully added
    } else {
        //so we should add articleWriters
        $writer_id = mysqli_insert_id($connection);
        $query = "INSERT INTO `articlewriters` (`article_id`,`writer_id`) ";
        $query .= "VALUES (";
        $query .= "'$article_id',";
        $query .= "'$writer_id'";
        $query .= ")";
        $res = mysqli_query($connection, $query);
        if ($res == false) {
            //if not successful show warn
            echo '
                <div class="alert">
                  <span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span> 
                  ' . mysqli_error($connection) . '
                </div>
        ';
        } else {
            //if successful show done!
            echo '
                <div class="info">
                  <span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span> 
                  اطلاعات نویسنده با موفقیت افزوده شد
                </div>
        ';
        }
    }
}
function retArticlesForExcutive($secretary_email, $type)
{
    global $connection;
    if($type == 0)
    {
                    $query = "SELECT articlesubmission.article_ID,articles.title,status FROM articlesubmission,articles where articlesubmission.article_id = articles.id and articlesubmission.secretary_email = '$secretary_email'";
    }
    else if($type == 1)
    {
                    $query = "SELECT articlesubmission.article_ID,articles.title,status FROM articlesubmission,articles where articlesubmission.article_id = articles.id and articlesubmission.secretary_email = '$secretary_email' and articles.status = 'در انتظار بررسی'";
    }
    else if ($type == 2)
    {
                   $query = "SELECT articlesubmission.article_ID,articles.title,status FROM articlesubmission,articles where articlesubmission.article_id = articles.id and articlesubmission.secretary_email = '$secretary_email' and articles.status = 'در حال داوری'";
  
    }
    else if($type == 3)
    {
                   $query = "SELECT articlesubmission.article_ID,articles.title,status FROM articlesubmission,articles where articlesubmission.article_id = articles.id and articlesubmission.secretary_email = '$secretary_email' and articles.status = 'پذیرش شده'";
    }   
    else if($type == 4)
    {
                   $query = "SELECT articlesubmission.article_ID,articles.title,status FROM articlesubmission,articles where articlesubmission.article_id = articles.id and articlesubmission.secretary_email = '$secretary_email' and articles.status = 'عدم پذیرش'";
    }
    $res = mysqli_query($connection, $query);
    if($res == false)
    {
        dberror();
    }
    else
    {
        for($i = 0;$i<mysqli_num_rows($res);$i++)
        {
            echo "<tr>";
            mysqli_data_seek($res,$i);
            $row = mysqli_fetch_row($res);
            $article_id = $row[0];
            $article_title = $row[1];
            $article_status = $row[2];
            echo "<td>$article_id</td>";
            echo "<td><a href=". returnPageofHere("showarticle")."&id=".$article_id.">$article_title </a></td>";
            echo "<td> $article_status</td>";
            echo "<td>
                    <a data-toggle='tooltip' title='ارسال به داوران' id='send_article_$article_id' onclick='SendArticle($article_id)' href=\"#\"><span class=\"glyphicon glyphicon-share-alt\"></span></a>
                    
                    
                  </td>";
            echo "</tr>";
        }
    }
}
function retArticlesForJudge($judge_email, $type)
{
    global $connection;
    if($type == 0)
    {
        $query = "SELECT articlesubmission.article_ID,articles.title,status FROM articlesubmission,articles where articlesubmission.article_id = articles.id and (articlesubmission.first_judge_email = '$judge_email' or articlesubmission.second_judge_email = '$judge_email' or articlesubmission.third_judge_email = '$judge_email') and articles.status = 'در حال داوری'";;
    }
    else if($type == 1)
    {
        $query = "SELECT articlesubmission.article_ID,articles.title,status FROM articlesubmission,articles where articlesubmission.article_id = articles.id and (articlesubmission.first_judge_email = '$judge_email' or articlesubmission.second_judge_email = '$judge_email' or articlesubmission.third_judge_email = '$judge_email') and articles.status = 'پذیرش شده'";
    }
    else if($type == 2)
    {
        $query = "SELECT articlesubmission.article_ID,articles.title,status FROM articlesubmission,articles where articlesubmission.article_id = articles.id and (articlesubmission.first_judge_email = '$judge_email' or articlesubmission.second_judge_email = '$judge_email' or articlesubmission.third_judge_email = '$judge_email') and articles.status = 'عدم پذیرش'";
    }
    $res = mysqli_query($connection, $query);
    if($res == false)
    {
        dberror();
    }
    else
    {
        for($i = 0;$i<mysqli_num_rows($res);$i++)
        {
            echo "<tr>";
            mysqli_data_seek($res,$i);
            $row = mysqli_fetch_row($res);
            $article_id = $row[0];
            $article_title = $row[1];
            $article_status = $row[2];
            echo "<td>$article_id</td>";
            echo "<td><a href=". returnPageofHere("showarticle")."&id=".$article_id.">$article_title </a></td>";
            echo "<td> $article_status</td>";
            echo "<td>
                    <a data-toggle='tooltip' title='تایید مقاله'  onclick='AcceptArticle($article_id)' href=\"#\"><span class=\"glyphicon glyphicon-ok\"></span></a>
                    <a data-toggle='tooltip' title='رد کردن مقاله'  onclick='RejectArticle($article_id)' href=\"#\"><span class=\"glyphicon glyphicon-remove\"></span></a>
                    
                  </td>";
            echo "</tr>";
        }
    }
}
function CheckFinalAccept($article_id)
{
    global $connection;
    $query = "SELECT first_judge_decision,second_judge_decision,third_judge_decision FROM articlesubmission where article_id = '" . $_POST['article_id'] . "'";
    $res = mysqli_query($connection, $query);
    if(mysqli_num_rows($res) == 1) // Check Article
    {
        mysqli_data_seek($res,0);
        $row = mysqli_fetch_row($res);
        if($row[0] == 'تایید' && $row[1] == 'تایید' && $row[2] == 'تایید' )
        {
            UpdateArticleStatus($_POST['article_id'], 3);
        }
        else if($row[0] == 'عدم تایید' || $row[1] == 'عدم تایید' || $row[2] == 'عدم تایید' )
        {
            UpdateArticleStatus($_POST['article_id'], 4);
        }
        else
        {
            UpdateArticleStatus($_POST['article_id'], 2);
        }
    }
}
function AcceptArticle()
{
    global $connection;
    $judge_email = $_SESSION['login_user'];
    $query = "SELECT article_id FROM articlesubmission where article_id = " . $_POST['article_id'] . " and articlesubmission.first_judge_email = '$judge_email' ";
    $res = mysqli_query($connection, $query);
    if(mysqli_num_rows($res) == 1) // First judge
    {
        $query = "UPDATE articlesubmission ";
        $query .= "set first_judge_decision= 'تایید' ";
        $query .= "where article_id = '". $_POST['article_id']."' ";
        $res = mysqli_query($connection, $query);
        if ($res == false) {
            dberror();
        } else {
            CheckFinalAccept($_POST['article_id']);
            echo '
                <div class="info">
                  <span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span> 
                    مقاله با موفقیت تایید گردید
                </div>
        ';
        }
        return;
    }
    $query = "SELECT article_id FROM articlesubmission where article_id = " . $_POST['article_id'] . " and articlesubmission.second_judge_email = '$judge_email' ";
    $res = mysqli_query($connection, $query);
    if(mysqli_num_rows($res) == 1) // Second judge
    {
        $query = "UPDATE articlesubmission ";
        $query .= "set second_judge_decision= 'تایید' ";
        $query .= "where article_id = '". $_POST['article_id']."' ";
        $res = mysqli_query($connection, $query);
        if ($res == false) {
            dberror();
        } else {
            CheckFinalAccept($_POST['article_id']);
            echo '
                <div class="info">
                  <span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span> 
                    مقاله با موفقیت تایید گردید
                </div>
        ';
        }
        return;
    }
    $query = "SELECT article_id FROM articlesubmission where article_id = " . $_POST['article_id'] . " and articlesubmission.third_judge_email = '$judge_email' ";
    $res = mysqli_query($connection, $query);
    if(mysqli_num_rows($res) == 1) // Third judge
    {

        $query = "UPDATE articlesubmission ";
        $query .= "set third_judge_decision= 'تایید' ";
        $query .= "where article_id = '". $_POST['article_id']."' ";
        $res = mysqli_query($connection, $query);
        if ($res == false) {
            dberror();
        } else {
            CheckFinalAccept($_POST['article_id']);
            echo '
                <div class="info">
                  <span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span> 
                    مقاله با موفقیت تایید گردید
                </div>
        ';
        }
        return;
    }
}
function RejectArticle()
{
    global $connection;
    $judge_email = $_SESSION['login_user'];
    $query = "SELECT article_id FROM articlesubmission where article_id = " . $_POST['article_id'] . " and articlesubmission.first_judge_email = '$judge_email' ";
    $res = mysqli_query($connection, $query);
    if(mysqli_num_rows($res) == 1) // First judge
    {
        $query = "UPDATE articlesubmission ";
        $query .= "set first_judge_decision= 'عدم تایید' ";
        $query .= "where article_id = '". $_POST['article_id']."' ";
        $res = mysqli_query($connection, $query);
        if ($res == false) {
            dberror();
        } else {
            CheckFinalAccept($_POST['article_id']);
            echo '
                <div class="info">
                  <span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span> 
                    مقاله با موفقیت رد شد
                </div>
        ';
        }
        return;
    }
    $query = "SELECT article_id FROM articlesubmission where article_id = " . $_POST['article_id'] . " and articlesubmission.second_judge_email = '$judge_email' ";
    $res = mysqli_query($connection, $query);
    if(mysqli_num_rows($res) == 1) // Second judge
    {
        $query = "UPDATE articlesubmission ";
        $query .= "set second_judge_decision= 'عدم تایید' ";
        $query .= "where article_id = '". $_POST['article_id']."' ";
        $res = mysqli_query($connection, $query);
        if ($res == false) {
            dberror();
        } else {
            CheckFinalAccept($_POST['article_id']);
            echo '
                <div class="info">
                  <span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span> 
                    مقاله با موفقیت رد شد
                </div>
        ';
        }
        return;
    }
    $query = "SELECT article_id FROM articlesubmission where article_id = " . $_POST['article_id'] . " and articlesubmission.third_judge_email = '$judge_email' ";
    $res = mysqli_query($connection, $query);
    if(mysqli_num_rows($res) == 1) // Third judge
    {
        $query = "UPDATE articlesubmission ";
        $query .= "set third_judge_decision= 'عدم تایید' ";
        $query .= "where article_id = '". $_POST['article_id']."' ";
        $res = mysqli_query($connection, $query);
        if ($res == false) {
            dberror();
        } else {
            CheckFinalAccept($_POST['article_id']);
            echo '
                <div class="info">
                  <span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span> 
                    مقاله با موفقیت رد شد
                </div>
        ';
        }
        return;
    }
}
function getJudges()
{
    global $connection;
    $query = "SELECT `name`,`family`,`email` FROM users WHERE users.title ='داور'";
    $res = mysqli_query($connection, $query);
    if ($res==true) {
        for($i = 0;$i<mysqli_num_rows($res);$i++)
        {
            mysqli_data_seek($res,$i);
            $row = mysqli_fetch_row($res);
            $judge_name = $row[0];
            $judge_family = $row[1];
            $judge_email = $row[2];
            echo "<option value='$judge_email'>$judge_name"." ". "$judge_family</option>";
        }
    }
}
function getExecutives()
{
    global $connection;
    $query = "SELECT `name`,`family`,`email` FROM users WHERE users.title ='دبیر'";
    $res = mysqli_query($connection, $query);
    if ($res==true) {
        for($i = 0;$i<mysqli_num_rows($res);$i++)
        {
            mysqli_data_seek($res,$i);
            $row = mysqli_fetch_row($res);
            $judge_name = $row[0];
            $judge_family = $row[1];
            $judge_email = $row[2];
            echo "<option value='$judge_email'>$judge_name"." ". "$judge_family</option>";
        }
    }
}
function admin_getuserslist()
{
    global $connection;
    $query = "SELECT `name`,`family`,`email`,`gender`,`degree`,`title` FROM users";
    $res = mysqli_query($connection, $query);
    if ($res==true) {
        for($i = 0;$i<mysqli_num_rows($res);$i++)
        {
            mysqli_data_seek($res,$i);
            $row = mysqli_fetch_row($res);
           ;
            $article_title = $row[1];
            $article_status = $row[2];
            echo "<tr>";
            echo "<td> $row[0]</td>";
            echo "<td> $row[1]</td>";
            echo "<td> $row[2]</td>";
            echo "<td> $row[3]</td>";
            echo "<td> $row[4]</td>";
            echo "<td> $row[5]</td>";
            echo "<td>

                    <a data-toggle=\"tooltip\" title=\"ارتقا به داور\" onclick='PromoteToJudge(\"$row[2]\")' href=\"#\"><span class=\"glyphicon glyphicon-sunglasses\"></span></a>
                    <a data-toggle=\"tooltip\" title=\"ارتقا به دبیر\" onclick='PromoteToExecutive(\"$row[2]\")' href=\"#\"><span class=\"glyphicon glyphicon-paperclip\"></span></a>
                    
                 </td>";
            echo "</tr>";
        }
    }
}
function retArticlesForManager($type)
{
    global $connection;
    if($type == 0)
    {
        $query = "SELECT ID,articles.title,status FROM articles";
    }
    else if($type == 1)
    {
        $query = "SELECT ID,articles.title,status FROM articles where  articles.status = 'در انتظار بررسی'";
    }
    else if ($type == 2)
    {
        $query = "SELECT ID,articles.title,status FROM articles where  articles.status = 'در حال داوری'";

    }
    else if($type == 3)
    {
        $query = "SELECT ID,articles.title,status FROM articles where  articles.status = 'پذیرش شده'";
    }
    else if($type == 4)
    {
        $query = "SELECT ID,articles.title,status FROM articles where  articles.status = 'عدم پذیرش'";
    }
    else if($type == 5)
    {
        $query = "SELECT ID,articles.title,status FROM articles where  articles.status = 'جدید'";
    }
    $res = mysqli_query($connection, $query);
    if($res == false)
    {
        dberror();
    }
    else
    {
        for($i = 0;$i<mysqli_num_rows($res);$i++)
        {
            echo "<tr>";
            mysqli_data_seek($res,$i);
            $row = mysqli_fetch_row($res);
            $article_id = $row[0];
            $article_title = $row[1];
            $article_status = $row[2];
            echo "<td>$article_id</td>";
            echo "<td><a href=". returnPageofHere("showarticle")."&id=".$article_id.">$article_title </a></td>";
            echo "<td> $article_status</td>";
            echo "<td>                   
                    <a data-toggle=\"tooltip\" title=\"ارسال پیام\" onclick='SendMessage($article_id)' href=\"#\"><span class=\"glyphicon glyphicon-envelope\"></span></a>
                    <a data-toggle=\"tooltip\" title=\"ارجاع به دبیر\" onclick='SendToExecutive($article_id)' href=\"#\"><span class=\"glyphicon glyphicon glyphicon-glass\"></span></a>           
                    </td>";
            echo "</tr>";
        }
    }
}
function admin_UpdateUserTitle()
{
    global $connection;

    $query = "UPDATE users ";
    $query .= "set title = '".$_POST['type'] ."'";
    $query .= "where email = '".$_POST['user'] ."'";
    $res = mysqli_query($connection, $query);
    if($res == true)
    {
        echo '
                <div class="info">
                  <span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span> 
                   کاربر با موفقیت ارتقا یافت.
                </div>
        ';
    }
    else
    {
        dberror();
    }
}
function UpdateArticleStatus($article_id,$type)
{
    global $connection;
    //give these items and check if there is answer to this query or not
    if($type == 1)$status = "در انتظار بررسی";
    else if($type == 2)$status = "در حال داوری";
    else if($type == 3)$status = "پذیرش شده";
    else if($type == 4)$status = "عدم پذیریش";
    else if($type == 5)$status = "جدید";
    $query = "UPDATE articles ";
    $query .= "set status = '$status'";
    $query .= "where ID = '$article_id' ";
    $res = mysqli_query($connection, $query);
    return $res;
}
function admin_submitArticleToExecutive(){
    global $connection;
    $query = "select article_id from articlesubmission where article_id = '". $_POST['article_id']."'";
    $res = mysqli_query($connection, $query);
    if(mysqli_num_rows($res) == 1)
    {
        $query = "UPDATE articlesubmission ";
        $query .= "set secretary_email = '" . $_POST['secretary_email'] . "'";
        $query .= "where article_id = '". $_POST['article_id']."' ";
        $res = mysqli_query($connection, $query);
        if ($res == false) {
            dberror();
        } else {
            if (UpdateArticleStatus($_POST['article_id'], 1)) {
                echo '
                <div class="info">
                  <span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span> 
                    مقاله مجددا به دبیر جدید ارسال شد .
                </div>
        ';
            } else {
                dberror();
            }
        }
    }
    else {
        $query = "INSERT INTO `articlesubmission` (`article_id`,`secretary_email`)";
        $query .= "VALUES (";
        $query .= "'" . $_POST['article_id'] . "',";
        $query .= "'" . $_POST['secretary_email'] . "'";
        $query .= ")";
        $res = mysqli_query($connection, $query);
        if ($res == false) {
            dberror();
        } else {
            if (UpdateArticleStatus($_POST['article_id'], 1)) {
                echo '
                <div class="info">
                  <span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span> 
                    مقاله با موفقیت به دبیر ارسال شد .
                </div>
        ';
            } else {
                dberror();
            }
        }
    }
}
function executive_submitArticleToJudges()
{
    global $connection;
    $query = "UPDATE articlesubmission ";
    $query .= "set first_judge_email = '" . $_POST['first_judge_email'] . "',";
    $query .= "second_judge_email = '" . $_POST['second_judge_email'] . "',";
    $query .= "third_judge_email = '" . $_POST['third_judge_email'] . "' ";
    $query .= "where article_id = '". $_POST['article_id']."' ";
    $res = mysqli_query($connection, $query);
    if ($res == false) {
        dberror();
    } else {
        if (UpdateArticleStatus($_POST['article_id'], 2)) {
            echo '
                <div class="info">
                  <span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span> 
                    مقاله مقاله به داوران ارسال شد .
                </div>
        ';
        } else {
            dberror();
        }
    }
}