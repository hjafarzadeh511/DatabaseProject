<?php
require_once 'db.php';
?>

<div class="container" style="margin-top:30px;max-height: 200px;max-width: 500px;direction:rtl">
    <div class="login-panel panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"> پنل کاربری </h3>
        </div>
        <div class="panel-body" >
            <!-- Centered Tabs -->
            <ul class="nav nav-tabs nav-justified">
                <li class="active"><a data-toggle="tab" href="#newarticle">ثبت مقاله جدید</a></li>
                <li><a data-toggle="tab" href="#newwriter">افزودن نویسنده</a></li>
                <li><a data-toggle="tab" href="#userarticles">لیست مقاله های شما</a></li>
                <li><a data-toggle="tab" href="#changeprofile">تغییر مشخصات کاربری</a></li>
            </ul>

            <div class="tab-content" >
                <div id="newarticle" class="tab-pane fade in active">
                    <?php
                    require 'newarticle.php';
                    ?>
                </div>
                <div id="newwriter" class="tab-pane fade in">
                    <?php
                    require 'newwriter.php';
                    ?>
                </div>
                <div id="userarticles" class="tab-pane fade">
                    <?php
                    require_once 'userarticlelist.php';
                    ?>
                 </div>
                <div id="changeprofile" class="tab-pane fade">
                    <?php
                    mysqli_query($connection, "SET NAMES 'utf8'");
                    mysqli_query( $connection,"SET CHARACTER SET 'utf8'");
                    mysqli_query( $connection,"SET character_set_connection = 'utf8'");
                    $query = "select password,name,family,gender,degree from users where email='". $_SESSION['login_user'] ."'";
                    $res = mysqli_query($connection, $query);
                    if($res == false) echo mysqli_error($res);
                    mysqli_data_seek($res, 0);
                    $profiledata = mysqli_fetch_row($res);
                    ?>
                    <form id="userupdateform" role="form" action='<?php returnPage("register") ?>' method="POST">
                        <fieldset>
                            <input type="hidden" name="command" value="updateuserinfo" />
                            <input type="hidden" name="email" value="<?php echo ($_SESSION['login_user']); ?>" />
                            <div class="form-group">
                            <p class="pull-right" style="margin-top: 10px">کلمه عبور</p>
                                <input class="form-control" placeholder="رمز ورود" name="password" type="password"
                                       value="<?php echo $profiledata[0];?>" required>
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="نام" name="name" type="text"
                                       value="<?php echo $profiledata[1];?>" required>
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="نام خانوادگی" name="family" type="text"
                                       value="<?php echo $profiledata[2];?>" required>
                            </div>
                            <div class="form-group">
                                <select class="form-control" name="gender" >
                                    <option value="مرد" <?php if($profiledata[3] =='مرد')echo 'selected';?> >مرد</option>
                                    <option value="زن"  <?php if($profiledata[3] =='زن')echo 'selected';?>>زن</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <select class="form-control" name="degree">
                                    <option value="زیر دیپلم" <?php if($profiledata[4] =='زیر دیپلم')echo 'selected';?>>زیر دیپلم</option>
                                    <option value="دیپلم" <?php if($profiledata[4] =='دیپلم')echo 'selected';?>>دیپلم</option>
                                    <option value="کارشناسی" <?php if($profiledata[4] =='کارشناسی')echo 'selected';?>>کارشناسی</option>
                                    <option value="کارشناسی ارشد" <?php if($profiledata[4] =='کارشناسی ارشد')echo 'selected';?>>کارشناسی ارشد</option>
                                    <option value="دکتری" <?php if($profiledata[4] =='دکتری')echo 'selected';?>>دکتری</option>
                                </select>
                            </div>
                            <div id="userupdateresult">

                            </div>
                            <input id="updatebtn" class="btn btn-sm btn-warning" value="به روز رسانی">
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/JavaScript">
    $(function () {
        $("#updatebtn").click(function (event) {
            event.preventDefault()
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("userupdateresult").innerHTML = this.responseText;
                }
            };
            xhttp.open("POST", "modules/dbcore.php", true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.send($("#userupdateform").serialize());
        });
    });
</script>
