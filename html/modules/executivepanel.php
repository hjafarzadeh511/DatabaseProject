<div class="container" style="margin-top:30px;max-height: 200px;max-width: 500px;direction:rtl">

    <!-- Modal -->
    <div id="executivemodal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title">ارسال مقاله به داوران</h4>
                </div>
                <div id="ModalBody" class="modal-body">
                    <form id="newwriterform" role="form" action='' method="POST">
                        <fieldset>
                            <div class="form-group">
                                <li class="pull-right">داور اول</li>
                                <select class="form-control" id="judge-1" >
                                    <div id="Judge1List">
                                        <?php getJudges(); ?>
                                    </div>
                                </select>
                            </div>
                            <div class="form-group">
                                <li class="pull-right"> داور دوم</li>
                                <select class="form-control" id="judge-2">
                                    <div id="Judge2List">
                                        <?php getJudges(); ?>
                                    </div>
                                </select>
                            </div>
                            <div class="form-group">
                                <li class="pull-right">داور سوم</li>
                                <select class="form-control" id="judge-3">
                                    <div id="Judge3List">
                                        <?php getJudges(); ?>
                                    </div>
                                </select>
                            </div>
                        </fieldset>
                    </form>
                <div id="judgesubmission">
                </div>
                </div>
                <div class="modal-footer">
                    <button id="SendArticleToJudges" onclick="SubmitArticleToJudges()" type="button" class="btn btn-success">ارسال</button>
                </div>
            </div>

        </div>
    </div>

    <div class="login-panel panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title" style="color: #0e1555;margin-bottom: 30px">پنل دبیر</h3>
            <button id="all" type="button" class="btn btn-default">همه مقالات</button>
            <button id="not_revised" type="button" class="btn btn-primary">مقالات بررسی نشده</button>
            <button id="during_revising" type="button" class="btn btn-warning">مقالات در حال داوری</button>
            <button id="accepted" type="button" class="btn btn-success">مقالات تایید شده</button>
            <button id="rejected" type="button" class="btn btn-danger">مقالات رد شده</button>
        </div>

        <div class="tab-content" >

        <div class="panel-body" style="min-width: 60vw">
            <table  id="articletable" class="table table-striped">
                <div id="tableheader" style="margin-top: 10px ;margin-bottom: 5px ">مقالات بررسی نشده</div>
                <thead>
                </thead>
                <thead style="text-align: center">
                <tr>
                    <th style="text-align: center">کد مقاله</th>
                    <th style="text-align: center">عنوان مقاله</th>
                    <th style="text-align: center">وضعیت</th>
                    <th style="text-align: center">عملیات</th>
                </tr>
                <tbody id="tablebody">
                <div id="tablediv">
                </div>
                </tbody>
                </thead>
            </table>
        </div>
        </div>
        </div>

    </div>
</div>




<script type="text/javascript">
    var selected_article;
    function SendArticle(article_id) {
        selected_article = article_id;
       $("#executivemodal").modal('show');
    }
    function SubmitArticleToJudges()
    {
        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", "modules/dbcore.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("command=submittojudges&first_judge_email=" + $("#judge-1").val() +"&second_judge_email=" + $("#judge-2").val() + "&third_judge_email=" + $("#judge-3").val() + "&article_id=" + selected_article);
        document.getElementById("judgesubmission").innerHTML = "در حال ارسال";
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("judgesubmission").innerHTML = this.responseText;
            }
        };
    }
    function retTable(type)
    {
        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", "modules/dbcore.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("command=retArticlesForExcutive&type=" + type);
        document.getElementById("tableheader").innerHTML = "در حال بارگزاری";
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
               if(type == 0) document.getElementById("tableheader").innerHTML = "همه مقالات";
               else  if(type == 1) document.getElementById("tableheader").innerHTML = "مقالات بررسی نشده";
               else  if(type == 2) document.getElementById("tableheader").innerHTML = "مقالات در حال داوری";
               else  if(type == 3) document.getElementById("tableheader").innerHTML = "مقالات تایید شده";
               else  if(type == 4) document.getElementById("tableheader").innerHTML = "مقالات رد شده";
                document.getElementById("tablebody").innerHTML = this.responseText;
            }
        };
    }

    $(document).ready(function() {
        retTable(0);
        $("#all").click( function () {
            retTable(0);
        });
        $("#not_revised").click( function () {
            retTable(1);
        });
    $("#during_revising").click( function () {
        retTable(2);
        });
    $("#accepted").click( function () {
        retTable(3);
        });
    $("#rejected").click( function () {
        retTable(4);
        });
    });
</script>