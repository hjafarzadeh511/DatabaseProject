<?php
require_once 'db.php';
?>

<div class="container" style="margin-top:30px;max-height: 200px;max-width: 500px;direction:rtl">
    <div class="login-panel panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"> ورود به پنل کاربری</h3>
        </div>
        <div class="panel-body" >
            <form role="form" action="" method="get">
                <fieldset>
                    <input type="hidden" name="p" value="login" />
                    <div class="form-group" >
                        <input class="form-control" placeholder="پست الکترونیکی شما" name="email" type="email"
                               autofocus="">
                    </div>
                    <div class="form-group">
                        <input class="form-control" placeholder="رمز ورود" name="password" type="password"
                               value="">
                    </div>
                    <p style="color: red; margin-top: 15px">
                        <?php
                        if(isset($_GET['p']) && $_GET['p'] == "login")
                        {
                            $query = "SELECT * FROM users WHERE users.email ='".$_GET['email']. "' AND users.`password` = '". $_GET['password'] . "'";
                            $res = mysqli_query($connection,$query);
                            if(mysqli_num_rows($res) == 1)
                            {
                                $_SESSION['login_user']=$_GET['email']; // Initializing Session
                            }
                            else{
                                echo "کاربری با مشخصات فوق وجود ندارد.";
                            }
                        }
                        ?>
                    <div class="checkbox" style="direction: ltr">
                        <label class="checkbox-inline">
                            <input  name="remember" type="checkbox" style="margin-top: 8px" >
                            <h style="color: #000"> من را به خاطر بسپار</h>
                        </label>
                    </div>


                    <input type="submit" class="btn btn-sm btn-success" value="ورود">
                    <input type="submit" class="btn btn-sm btn-primary" value="بازیابی کلمه عبور">


                </fieldset>
            </form>
        </div>
    </div>
</div>

