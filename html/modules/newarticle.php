<form id="articleform" role="form" action="./upload.php" method="post" enctype="multipart/form-data">
    <fieldset>
        <input type="hidden" name="p" value="newarticle" />
        <input type="hidden" name="email" value="<?php echo $_SESSION['login_user'] ?>" />
        <div class="form-group" >
            <input class="form-control" style="margin-top: 1vw" placeholder="عنوان مقاله" id="title" name="title" type="text" autofocus="" required>
        </div>
        <div class="form-group">
            <input class="form-control" placeholder="کلمات کلیدی"  id="keywords" name="keywords" type="text" value="" required>
        </div>
        <div class="form-group">
            <textarea class="form-control"  id="abstract_fa" name="abstract_fa" placeholder="چکیده را اینجا وارد کنید" form="articleform" required></textarea>
        </div>
        <div class="form-group">
            <textarea class="form-control"  id="abstract_en" name="abstract_en" style="direction: ltr" placeholder="Enter english abstract here..." required form="articleform"></textarea>
        </div>
        <div class="form-group">
            فایل مقاله :
              <input class="form-control" type="file" name="fileToUpload" id="fileToUpload" >
        </div>
        <div id="result">

        </div>
        <input id="submitbutton"  type="submit" class="btn btn-sm btn-success" value="افزودن مقاله">


    </fieldset>
</form>

<script type="text/JavaScript">
    $(function () {
        $("#submitbutton").click(function(event) {
            event.preventDefault();
            if(!$("#title").val().length || !$("#keywords").val().length || !$("#abstract_en").val().length || !$("#abstract_fa").val().length)
            {
                document.getElementById("result").innerHTML ="<div class=\"alert\"><span class=\"closebtn\" onclick=\"this.parentElement.style.display=\'none\';\">&times;</span>خطا در ورودی ها </div>";
                return;
            }
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("result").innerHTML = this.responseText;
                }
            };
            xhttp.open("POST", "/modules/commitarticle.php", true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.send($("#articleform").serialize());

        });
    });
</script>
