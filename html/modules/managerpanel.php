<div class="container" style="margin-top:30px;max-height: 200px;max-width: 500px;direction:rtl">

<div id="operationstatus">
</div>
    <!-- Executive Modal -->
    <div id="executivemodal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title">ارسال مقاله به دبیر</h4>
                </div>
                <div id="ModalBody" class="modal-body">
                    <form class="form-inline" role="form" action='' method="POST">
                        <fieldset>
                            <div class="form-group">
                                مقاله شماره :
                            <input class="form-control" id="article_id_no" type="text" value="hello">
                            </div>
                            <div class="form-group">
                                دبیر مورد نظر:
                                <select class="form-control" id="executive_email" name="executive" >
                                    <div id="executivelist">
                                        <?php getExecutives(); ?>
                                    </div>
                                </select>
                            </div>
                        </fieldset>
                    </form>
                </div>
                <div id="submissionstatus">
                </div>
                <div class="modal-footer">
                    <button onclick="SubmitSendToExecutive()" type="button" class="btn btn-success">ارسال</button>
                </div>
            </div>

        </div>
    </div>

    <div class="login-panel panel panel-success" style="max-height: 35vw;">
        <div class="panel-heading" >
            <h3 class="panel-title" style="color: #0e1555">پنل مدیریت</h3>
        </div>


            <div class="panel-body" style="min-width: 60vw;max-height: 30vw;overflow-y: scroll;">
                <ul class="nav nav-tabs nav-justified">
                    <li class="active"><a data-toggle="tab" href="#articleslist">مدیریت مقالات ثبت شده</a></li>
                    <li><a data-toggle="tab" href="#userlist">مدیریت کاربران</a></li>
                    <li><a data-toggle="tab" href="#systemconfig">تنظیمات سامانه</a></li>
                    <li><a data-toggle="tab" href="#changeprofile">تغییر مشخصات کاربری</a></li>
                </ul>

                <div class="tab-content" >
                    <div id="articleslist" class="tab-pane fade in active">
                        <button id="all" type="button" class="btn btn-default">همه مقالات</button>
                        <button id="new_articles" type="button" class="btn btn-primary">مقالات جدید</button>
                        <button id="not_revised" type="button" class="btn btn-primary">مقالات بررسی نشده</button>
                        <button id="during_revising" type="button" class="btn btn-warning">مقالات در حال داوری</button>
                        <button id="accepted" type="button" class="btn btn-success">مقالات تایید شده</button>
                        <button id="rejected" type="button" class="btn btn-danger">مقالات رد شده</button>
                        <table  id="articletable" class="table table-striped">
                            <div id="tableheader" style="margin-top: 10px ;margin-bottom: 5px ">مقالات بررسی نشده</div>
                            <thead>
                            </thead>
                            <thead style="text-align: center">
                            <tr>
                                <th style="text-align: center">کد مقاله</th>
                                <th style="text-align: center">عنوان مقاله</th>
                                <th style="text-align: center">وضعیت</th>
                                <th style="text-align: center">عملیات</th>
                            </tr>
                            <tbody id="tablebody">
                            <div id="tablediv">
                            </div>
                            </tbody>
                            </thead>
                        </table>
                    </div>
                    <div id="userlist" class="tab-pane fade in">
                        <table  id="articletable" class="table table-striped">
                            <div id="tableheader" style="margin-top: 10px ;margin-bottom: 5px ">مقالات بررسی نشده</div>
                            <thead>
                            </thead>
                            <thead style="text-align: center">
                            <tr>
                                <th style="text-align: center">نام</th>
                                <th style="text-align: center">نام خانوادگی</th>
                                <th style="text-align: center">ایمیل</th>
                                <th style="text-align: center">جنسیت</th>
                                <th style="text-align: center">تحصیلات</th>
                                <th style="text-align: center">کاربر خاص</th>
                                <th style="text-align: center">عملیات</th>
                                <?php admin_getuserslist(); ?>
                            </tr>
                            <tbody id="tablebody">
                            <div id="tablediv">
                            </div>
                            </tbody>
                            </thead>
                        </table>
                    </div>
                    <div id="userarticles" class="tab-pane fade">
                        <?php
                        require_once 'userarticlelist.php';
                        ?>
                    </div>
                    <div id="changeprofile" class="tab-pane fade">


                    </div>
                </div>

            </div>
    </div>

</div>
</div>

<script type="text/javascript">
    function SubmitSendToExecutive() {
        var article_id = $("#article_id_no").val();
        var secretary_email = $("#executive_email").val();

        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", "modules/dbcore.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("command=SubmitArticleToExecutive&article_id="+ article_id +"&secretary_email=" + secretary_email);
        document.getElementById("submissionstatus").innerHTML = "در حال ارسال";
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("submissionstatus").innerHTML= this.responseText;
                retTable(0);
            }
        };
    }
    function PromoteToJudge(user_email)
    {
        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", "modules/dbcore.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("command=promoteuser&user="+ user_email +"&type=داور");
        document.getElementById("operationstatus").innerHTML = "در حال بارگزاری";
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("operationstatus").innerHTML = this.response;
            }
        };
    }
    function PromoteToExecutive(user_email)
    {
        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", "modules/dbcore.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("command=promoteuser&user="+ user_email +"&type=دبیر");
        document.getElementById("operationstatus").innerHTML = "در حال بارگزاری";
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("operationstatus").innerHTML = this.response;
            }
        };
    }
    function SendToExecutive(article_id) {
        $("#article_id_no").val(article_id);
        $("#executivemodal").modal('show');

    }
    function retTable(type)
    {
        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", "modules/dbcore.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("command=retArticlesForManager&type=" + type);
        document.getElementById("tableheader").innerHTML = "در حال بارگزاری";
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                if(type == 0) document.getElementById("tableheader").innerHTML = "همه مقالات";
                else  if(type == 1) document.getElementById("tableheader").innerHTML = "مقالات بررسی نشده";
                else  if(type == 2) document.getElementById("tableheader").innerHTML = "مقالات در حال داوری";
                else  if(type == 3) document.getElementById("tableheader").innerHTML = "مقالات تایید شده";
                else  if(type == 4) document.getElementById("tableheader").innerHTML = "مقالات رد شده";
                else  if(type == 5) document.getElementById("tableheader").innerHTML = "مقالات جدید";
                document.getElementById("tablebody").innerHTML = this.responseText;
            }
        };
    }

    $(document).ready(function() {
        retTable(0);
        $("#all").click( function () {
            retTable(0);
        });
        $("#new_articles").click( function () {
            retTable(5);
        });
        $("#not_revised").click( function () {
            retTable(1);
        });
        $("#during_revising").click( function () {
            retTable(2);
        });
        $("#accepted").click( function () {
            retTable(3);
        });
        $("#rejected").click( function () {
            retTable(4);
        });
    });
</script>