<?php
require_once 'db.php';
?>
<form id="newwriterform" role="form" action='' method="POST">
    <fieldset>
        <div class="form-group" style="margin: 10px">
        لطفا مقاله ای که تمایل دارید نویسندگان آن را ویرایش کنید انتخاب نمایید.
        <select class="form-control" name="article_id">
        <?php
        $query = "select ID,title from articles where useremail='". $_SESSION['login_user'] ."'";
        $res = mysqli_query($connection, $query);
        for($i = 0;$i<mysqli_num_rows($res);$i++)
        {
            mysqli_data_seek($res,$i);
            $row = mysqli_fetch_row($res);
            $article_id = $row[0];
            $article_title = $row[1];
            echo "<option value='$article_id'>$article_id - $article_title</option>";
        }
        ?>
            </select>
        </div>
        <input name="command" value="addwriter" type="hidden" />
        <div class="form-group">
            <input class="form-control" placeholder="نام" name="writer_name" type="text"
                   value="">
        </div>
        <div class="form-group">
            <input class="form-control" placeholder="نام خانوادگی" name="writer_family" type="text"
                   value="">
        </div>
        <div class="form-group">
            <input class="form-control" placeholder="پست الکترونیکی " name="writer_email" type="email"
                   autofocus="">
        </div>
        <div class="form-group">
            <select class="form-control" name="writer_degree">
                <option value="زیر دیپلم">زیر دیپلم</option>
                <option value="دیپلم">دیپلم</option>
                <option value="کارشناسی">کارشناسی</option>
                <option value="کارشناسی ارشد">کارشناسی ارشد</option>
                <option value="دکتری">دکتری</option>
            </select>
        </div>
        <div class="form-group">
            <input class="form-control" placeholder="وابستگی به شرکت/سازمان/دانشگاه" name="writer_affiliation" type="text" required>
        </div>
         <div id="newwriterresult"></div>
        <input id="newwritersubmit" type="submit" class="btn btn-sm btn-success" value="افزودن نویسنده">


    </fieldset>
</form>

<script type="text/JavaScript">
    $(function () {
        $("#newwritersubmit").click(function () {
            event.preventDefault()
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("newwriterresult").innerHTML = this.responseText;
                }
            };
            xhttp.open("POST", "modules/dbcore.php", true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.send($("#newwriterform").serialize());
        });
    });
</script>
