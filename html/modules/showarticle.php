<?php
//echo "TODO:";
//echo "<p>check if user is authenticate to view this page</p>";
//echo "Show Article page,";
//echo "Dont forget to add edit and delete buttons in here";
//echo "<p> Different page contents for executive,judge and users </p>";
//title,keywords,abstract_fa,abstract_en,url,status
//
$article_fields = getArticleFields($_GET['id']);
$article_title = $article_fields[0];
$article_keywords = $article_fields[1];
$article_abstract_fa= $article_fields[2];
$article_abstract_en = $article_fields[3];
$article_url = $article_fields[4];
$article_status = $article_fields[5];


?>
<div class="container" style="margin-top:30px;max-height: 200px;max-width: 500px;direction:rtl">
<div class="login-panel panel panel-warning">
        <div class="panel-heading">
            <h3 class="panel-title" style="color: #0e1555;margin-bottom: 30px"><?php echo $article_title; ?></h3>
            <h6 class="btn btn-success"><?php echo $article_status; ?></h6>
        </div>

        <div class="tab-content" >

        <div class="panel-body" style="min-width: 60vw">
            <div >
                <?php
                if(CheckLogin()==false && $article_status != "پذیرش شده")
                {
                    echo "شما مجاز به دیدن مقالات غیر پذیرش شده نیستید";
                }
                else if(CheckLogin()==true && getUserValue("title") == "" && $article_status != "پذیرش شده")
                {
                    echo "شما تنها مجاز به دیدن مقالات چاپ شده هستید.";
                }
                else
                {
                    echo "<div>";
                    echo "خلاصه مقاله فارسی:  ";
                    echo "</div>";

                    echo $article_abstract_fa;
                    echo "<div>";
                    echo "خلاصه مقاله انگلیسی:  ";
                    echo "</div>";
                    echo $article_abstract_en;
                    echo "<div>";
                    echo "<h3> برای دانلود اصل مقاله کلیک کنید </h3>";
                    echo "</div>";
                }
                ?>
            </div>
        </div>
        </div>
        </div>
</div>