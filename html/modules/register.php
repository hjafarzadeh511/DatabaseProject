<?php
require_once 'db.php';
if (isset($_POST['name'])) {
    $regsucceded = false;
    $query = "INSERT INTO `users` (`email`, `password`, `username`, `name`, `family`, `gender`, `degree`, `title`) VALUES (N'" . $_POST['email'] . "', N'" . $_POST['password'] . "', N'" . $_POST['email'] . "', N'" . $_POST['name'] . "', N'" . $_POST['family'] . "', N'" . $_POST['gender'] . "', N'" . $_POST['degree'] . "', NULL)";
    $res = mysqli_query($connection, $query);
    if ($res == false) {
        echo '
                <div class="alert">
                  <span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span> 
                  '.mysqli_error($connection) .'
                </div>
        ';
    }
    else if ($res == true) {
        echo '
                <div class="info">
                  <span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span> 
       ثبت نام با موفقیت انجام شد. تا چند لحظه دیگر به صفحه ورود منتقل خواهید .
                </div>
                <script type="text/JavaScript">
                setTimeout(function () {
                        window.location.href = "?p=loginpage"; //will redirect to your blog page (an ex: blog.html)
                }, 4000);
                </script>
        ';
    }
}
?>
<div class="container" style="margin-top:30px;max-height: 200px;max-width: 500px;direction:rtl">
    <div class="login-panel panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">ثبت نام</h3>
        </div>
        <div class="panel-body">

            <form role="form" action='<?php returnPage("register") ?>' method="POST">
                <fieldset>
                    <div class="form-group">
                        <input class="form-control" placeholder="پست الکترونیکی شما" name="email" type="email"
                               autofocus="" required>
                    </div>
                    <div class="form-group">
                        <input class="form-control" placeholder="رمز ورود" name="password" type="password"
                               value="" required>
                    </div>
                    <div class="form-group">
                        <input class="form-control" placeholder="نام" name="name" type="text"
                               value="" required>
                    </div>
                    <div class="form-group">
                        <input class="form-control" placeholder="نام خانوادگی" name="family" type="text"
                               value="" required>
                    </div>
                    <div class="form-group">
                        <select class="form-control" name="gender">
                            <option value="مرد">مرد</option>
                            <option value="زن">زن</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control" name="degree">
                            <option value="زیر دیپلم">زیر دیپلم</option>
                            <option value="دیپلم">دیپلم</option>
                            <option value="کارشناسی">کارشناسی</option>
                            <option value="کارشناسی ارشد">کارشناسی ارشد</option>
                            <option value="دکتری">دکتری</option>
                        </select>
                    </div>

                    <input type="submit" class="btn btn-sm btn-success" value="ثبت نام">
                    <a href="<?php echo returnPage("loginpage") ?>" class="btn btn-sm btn-primary">قبلا ثبت نام کرده
                        ام</a>


                </fieldset>
            </form>
        </div>
    </div>
</div>