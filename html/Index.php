<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Conference - Responsive HTML5 Template</title>

    <!-- favicon -->
    <link href="favicon.png" rel=icon>

    <!-- web-fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,500' rel='stylesheet' type='text/css'>
    <link href='css/hjcss.css' rel='stylesheet' type='text/css'>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id="page-top" data-spy="scroll" data-target=".navbar">
<div id="main-wrapper">
    <!-- Page Preloader -->
    <div id="preloader">
        <div id="status">
            <div class="status-mes"></div>
        </div>
    </div>

    <header class="header">
        <!-- Navigation -->
        <nav class="navbar main-menu" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand page-scroll" href="#page-top"><img src="img/smartcity6.png" alt=""></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                    <ul class="nav navbar-nav">
                        <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                        <li class="hidden"><a href="#page-top"></a></li>
                        <li><a class="page-scroll" href="#section-venue">محل برگزاری</a></li>
                        <li><a class="page-scroll" href="#section-partner">حامیان</a></li>
                        <li><a class="page-scroll" href="#section-pricing">ثبت نام</a></li>
                        <li><a class="page-scroll" href="#section-ajenda">تقویم برگزاری</a></li>
                        <li><a class="page-scroll" href="#section-speaker">سخنرانان</a></li>
                        <li><a class="page-scroll" href="#section-intro">محور های همایش</a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>

        <!-- .nav -->

    </header>

    <div class="jumbotron text-center">
        <div class="content">
            <div class="event-date">21 فروردین 1396 </div>
            <h1>اولين همايش شهر هوشمند و اينترنت اشياء</h1>
            <p class="lead">«اینترنت اشیاء» فناوری مدرنی است که در آن برای هر موجودی (انسان، حیوان و یا اشیاء) قابلیت ارسال داده ازطریق شبکه‌های ارتباطی، اعم از اینترنت یا انواع دیگر، فراهم می‌گردد.. اینترنت اشیاء یک فناوری در حال ظهور و گسترش است که این ظهور و گسترش هر دو حوزه­ی تحقیقات، و کاربرد را شامل میشود. بنابر برآوردها، در سال ۲۰۲۰ بازار IoT به بالاتر از ۳۰۰ میلیارد دلار در سال خواهد رسید. در آینده نزدیک، حضور اینترنت اشیا در زندگی روزمره بسیار محسوس خواهد بود. بنابراین و با توجه به نوپایی این حوزه، سرمایه گذاری علمی و فنی در آن می­تواند باعث ایجاد مشاغل نو گردد. همچنین تحقیقات علمی در این حوزه در ایران و جهان به سرعت در حال رشد است.</p>
            <button type="button" class="hjbtn">ثبت نام در رویداد</button>
        </div>
    </div>
    <!-- .Jumbotron-->


    <section id="section-intro" class="section-wrapper about-event">
        <div class="container">
            <div class="section-title">
                <h1>محور های همایش</h1>
            </div>
            <h1 style="text-align: justify; direction: rtl; unicode-bidi: embed;">
                <p class=" " dir="rtl" style="line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;"><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="AR-SA">شهر هوشمند با استفاده از تکنولوژي ارتباطات و اطلاعات</span><span dir="ltr" style="font-size: 12pt; font-family: Tahoma,sans-serif; color: #535a24;"> </span><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="AR-SA">در نظر دارد تا کيفيت زندگي شهروندان
شهرها را بهبود دهد</span><span dir="ltr" style="font-size: 12pt; font-family: Tahoma,sans-serif; color: #535a24;">.</span><span dir="ltr" style="font-size: 10pt; font-family: Tahoma,sans-serif; color: #535a24;"></span></p>
                <p class=" " dir="rtl" style="line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;"><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="AR-SA">يک شهر هوشمند سه هدف مهم را دنبال مي کند: 1)جمع آوري داده ها 2)برقراري
ارتباط 3)آناليز داده</span><span style="font-size: 10pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="AR-SA"></span></p>
                <p class=" " dir="rtl" style="line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;"><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="AR-SA">تجهيزات هوشمند در سرتاسر شهر قرار دارند تا شرايط و اوضاع را اندازه گيري
و بررسي کنند. به عنوان مثال، کنتور هاي هوشمند مي توانند ميزان مصرف برق، گاز، آب
را اندازه گيري کنند. حسگرهاي ترافيکي هوشمند مي توانند شرايط جاده ها و ازدحام آن
ها را گزارش دهند</span><span dir="ltr" style="font-size: 12pt; font-family: Tahoma,sans-serif; color: #535a24;">. GPS&nbsp;</span><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="AR-SA">هاي هوشمند، مکان دقيق شهر، خيابان،
ترافيک و.... را گزارش مي دهند. همچنين ايستگاه هاي هواشناسي مي توانند شرايط جوي
را گزارش دهند. تلفن هاي هوشمند که توسط افراد زيادي استفاده مي شود، شامل
حسگرهايي است که</span><span style="font-size: 12pt; font-family: &amp;quot;Times New Roman&amp;quot;,serif; color: #535a24;" lang="AR-SA"> </span><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="AR-SA"> مي تواند سرعت، فشارخون و ساير پارامترهاي سلامتي را اندازه گيري کند.
وقتي که اين داده ها جمع آوري شد نياز به رسانه اي براي انتقال آنها به مقصد مي
باشد. براي اين کار از شبکه هاي ارتباطي باسيم يا بيسيم مانند فيبر نوري،</span><span style="font-size: 12pt; font-family: &amp;quot;Times New Roman&amp;quot;,serif; color: #535a24;" lang="AR-SA"> </span><span dir="ltr" style="font-size: 12pt; font-family: Tahoma,sans-serif; color: #535a24;">GPRS</span><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="AR-SA">،
کابل هاي مسي و.... استفاده مي شود. بعد از جمع آوري و برقراري ارتباط داده ها،
بايد شروع به آناليز آنها کرد.</span><span style="font-size: 10pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="AR-SA"></span></p>
                <p class=" " dir="rtl" style="line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;"><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="FA">هفت تکنولوژي باعث به وجود آمدن صفت
&quot;هوشمند&quot; براي شهرها شده اند. اين هفت تکنولوژي به شرح زير مي باشند:</span><span style="font-size: 10pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="AR-SA"></span></p>
                <p class=" " dir="rtl" style="margin-right: 0.5in; text-indent: -0.25in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;"><span dir="ltr" style="font-size: 12pt; font-family: Symbol; color: #535a24;">·&nbsp;</span><strong><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="FA">کنترل و ابزار دقيق:</span></strong><span style="font-size: 12pt; font-family: &amp;quot;Times New Roman&amp;quot;,serif; color: #535a24;" lang="FA"> </span><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="FA">از ابزارها براي
کنترل و نظارت بر تکنولوژي هاي موجود</span><span style="font-size: 12pt; font-family: &amp;quot;Times New Roman&amp;quot;,serif; color: #535a24;" lang="FA"> </span><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="FA"> استفاده مي شود. در
واقع اين ابزارهاي کنترلي در حکم چشم و گوش شهر مي باشند. برخي از اين ابزارها
عبارتند از:</span><span style="font-size: 10pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="AR-SA"></span></p>
                <p class=" " dir="rtl" style="margin-right: 1.5in; text-indent: -0.25in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;"><span dir="ltr" style="font-size: 12pt; font-family: Wingdings; color: #535a24;">§&nbsp;</span><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="FA">کنتورهاي هوشمند آب، برق و گاز</span><span style="font-size: 10pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="AR-SA"></span></p>
                <p class=" " dir="rtl" style="margin-right: 1.5in; text-indent: -0.25in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;"><span dir="ltr" style="font-size: 12pt; font-family: Wingdings; color: #535a24;">§&nbsp;</span><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="FA">سنسورهاي کيفيت هوا</span><span style="font-size: 10pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="AR-SA"></span></p>
                <p class=" " dir="rtl" style="margin-right: 1.5in; text-indent: -0.25in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;"><span dir="ltr" style="font-size: 12pt; font-family: Wingdings; color: #535a24;">§&nbsp;</span><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="FA">دوربين هاي مدار بسته</span><span style="font-size: 10pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="AR-SA"></span></p>
                <p class=" " dir="rtl" style="margin-right: 1.5in; text-indent: -0.25in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;"><span dir="ltr" style="font-size: 12pt; font-family: Wingdings; color: #535a24;">§&nbsp;</span><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="FA">سنسورهاي جاده اي</span><span style="font-size: 10pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="AR-SA"></span></p>
                <p class=" " dir="rtl" style="line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;"><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="FA">اين ابزارها براي کنترل و مديريت از راه دور بسيار
مناسب اند.</span><span style="font-size: 10pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="AR-SA"></span></p>
                <p class=" " dir="rtl" style="margin-right: 0.5in; text-indent: -0.25in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;"><span dir="ltr" style="font-size: 12pt; font-family: Symbol; color: #535a24;">·&nbsp;</span><strong><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="FA">شبکه هاي ارتباطي و برقراري اتصال :</span></strong><span style="font-size: 12pt; font-family: &amp;quot;Times New Roman&amp;quot;,serif; color: #535a24;" lang="FA"> </span><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="FA">ارتباط دستگاه ها با يکديگر و با مرکز داده ها در
شهر هاي هوشمند، ازاهميت</span><span style="font-size: 12pt; font-family: &amp;quot;Times New Roman&amp;quot;,serif; color: #535a24;" lang="FA"> </span><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="FA"> بالايي برخوردار
است. داده هاي جمع آوري شده توسط اين دستگاه ها مورد پردازش و بررسي قرار مي
گيرند. شبکه هاي ارتباطي مختلفي براي اين منظور استفاده مي شوند که مي توان به
موارد زير اشاره نمود:</span><span style="font-size: 10pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="AR-SA"></span></p>
                <p class=" " dir="rtl" style="margin-right: 1in; text-indent: -0.25in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;"><span dir="ltr" style="font-size: 12pt; font-family: Tahoma,sans-serif; color: #535a24;">o</span><span style="font-size: 12pt; font-family: &amp;quot;Times New Roman&amp;quot;,serif; color: #535a24;" lang="AR-SA"> </span><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="FA">شبکه</span><span style="font-size: 12pt; font-family: &amp;quot;Times New Roman&amp;quot;,serif; color: #535a24;" lang="FA"> </span><span dir="ltr" style="font-size: 12pt; font-family: Tahoma,sans-serif; color: #535a24;">Wi-Fi&nbsp;</span><span style="font-size: 12pt; font-family: &amp;quot;Times New Roman&amp;quot;,serif; color: #535a24;" lang="FA"> </span><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="FA">شهري</span><span style="font-size: 10pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="AR-SA"></span></p>
                <p class=" " dir="rtl" style="margin-right: 1in; text-indent: -0.25in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;"><span dir="ltr" style="font-size: 12pt; font-family: Tahoma,sans-serif; color: #535a24;">o</span><span style="font-size: 12pt; font-family: &amp;quot;Times New Roman&amp;quot;,serif; color: #535a24;" lang="AR-SA"> </span><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="FA">شبکه هاي</span><span style="font-size: 12pt; font-family: &amp;quot;Times New Roman&amp;quot;,serif; color: #535a24;" lang="FA"> </span><span dir="ltr" style="font-size: 12pt; font-family: Tahoma,sans-serif; color: #535a24;">Rf-mesh</span><span style="font-size: 10pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="AR-SA"></span></p>
                <p class=" " dir="rtl" style="margin-right: 1in; text-indent: -0.25in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;"><span dir="ltr" style="font-size: 12pt; font-family: Tahoma,sans-serif; color: #535a24;">o</span><span style="font-size: 12pt; font-family: &amp;quot;Times New Roman&amp;quot;,serif; color: #535a24;" lang="AR-SA"> </span><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="FA">شبکه هاي سلولي</span><span style="font-size: 10pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="AR-SA"></span></p>
                <p class=" " dir="rtl" style="margin-right: 0.5in; text-indent: -0.25in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;"><span dir="ltr" style="font-size: 12pt; font-family: Symbol; color: #535a24;">·&nbsp;</span><strong><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="FA">همکاري بين نهادها و اعضاي شهر:</span></strong><span style="font-size: 12pt; font-family: &amp;quot;Times New Roman&amp;quot;,serif; color: #535a24;" lang="FA"> </span><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="FA">اين سيستم ما را از همکاري کليه تکنولوژي هاي شهر
مطمئن مي سازد. يکي از مزيت هاي آن جلوگيري از تداخل فعاليت هاي شهر و بلاک شدن آن
مي باشد. همچنين به يک شهر اجازه انتخاب وشروع فعاليت هاي عمراني را با محاسبه
ضريب اطمينان مي دهد.</span><span style="font-size: 10pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="AR-SA"></span></p>
                <p class=" " dir="rtl" style="margin-right: 0.5in; text-indent: -0.25in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;"><span dir="ltr" style="font-size: 12pt; font-family: Symbol; color: #535a24;">·&nbsp;</span><strong><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="FA">امنيت و پنهان سازي:</span></strong><strong><span style="font-size: 12pt; font-family: &amp;quot;Times New Roman&amp;quot;,serif; color: #535a24;" lang="FA"> </span></strong><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="FA">از جمله روش ها و
سياست هاي امنيت داده ها، پنهان سازي آن ها مي باشد. امنيت داده ها نقشي حياتي و
مهم در ايجاد امنيت بين افراد در شهرهاي هوشمند بازي مي کند.</span><span style="font-size: 10pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="AR-SA"></span></p>
                <p class=" " dir="rtl" style="margin-right: 0.5in; text-indent: -0.25in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;"><span dir="ltr" style="font-size: 12pt; font-family: Symbol; color: #535a24;">·&nbsp;</span><strong><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="FA">مديريت داده ها</span></strong><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="FA">: داده ها، يکي از
مهره هاي اصلي در شکل گيري شهرهاي هوشمند مي باشد. مديريت داده ها و دسته بندي
درست آن ها تاثير بسزايي براي شکل گيري شهرهاي هوشمند دارد.</span><span style="font-size: 10pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="AR-SA"></span></p>
                <p class=" " dir="rtl" style="margin-right: 0.5in; text-indent: -0.25in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;"><span dir="ltr" style="font-size: 12pt; font-family: Symbol; color: #535a24;">·&nbsp;</span><strong><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="FA">محاسبه منابع</span></strong><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="FA">:</span><span style="font-size: 12pt; font-family: &amp;quot;Times New Roman&amp;quot;,serif; color: #535a24;" lang="FA"> </span><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="FA"> در درجه اول شامل کامپيوترهاي قدرتمند و سپس داده هاي
جمع آوري شده و نيازهاي مورد درخواست شهر هوشمند مي شود. امروزه شهر هاي هوشمند به
سوي توسعه پردازش ابري مي روند. با استفاده از محاسبات ابري نيازي به تهيه سيستم
هاي با قدرت پردازش بالا نمي باشد.</span><span style="font-size: 10pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="AR-SA"></span></p>
                <p class=" " dir="rtl" style="margin-right: 0.5in; text-indent: -0.25in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;"><span dir="ltr" style="font-size: 12pt; font-family: Symbol; color: #535a24;">·&nbsp;</span><strong><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="FA">آناليز</span></strong><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="FA">: داده هاي جمع آوري شده در مرکز داده، بايد تجزيه و
تحليل گردند. برخي از اين تجزيه و تحليل ها عبارتند از: پيش بيني وضع هوا، آناليز
مصرف انرژي برق و اينکه چه زماني توليد برق را افزايش يا کاهش داد، آناليز شرايط
براي اينکه چه قطعاتي نياز به تعمير دارند و مسيريابي هوشمند ترافيک جاده ها. در
واقع تمام اين پيش بيني ها براي ايجاد شرايط بهتر و مطلوب تر استفاده مي شود.</span><span style="font-size: 10pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="AR-SA"></span></p>
                <p><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="AR-SA">دراين همايش يک روزه، مباحث مختلف با
شهرهاي هوشمند و اينترنت اشياء بحث و بررسي مي شوند. </span><br /><span style="font-size: 12pt; font-family: &amp;quot;B Nazanin&amp;quot;; color: #535a24;" lang="AR-SA">مجموعه مقالات پذيرفته شده در اولين همايش &quot;شهر هوشمند و اينترنت اشيا&quot; در<strong> پايگاه داده استنادي علوم جهان اسلام (ISC) </strong>نمايه خواهند شد.</span></p>
                <p> </p>
            </h1>

        </div>
    </section>
    <!-- .about-event -->

    <section id="section-speaker" class="section-wrapper team gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h1>سخنرانان</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <figure class="thumbnail">
                        <img src="img/garcia.jpg" class="img-responsive" alt="Image">
                        <figcaption class="caption text-center">
                            <h3>Professor Alberto Leon Garcia
                                <small>University of Torento</small>
                            </h3>
                            <ul class="social-links">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-envelope"></i></a></li>
                            </ul>
                        </figcaption>
                    </figure>
                </div>
                <!-- /.col-sm-6 -->

                <div class="col-sm-6 col-md-3">
                    <figure class="thumbnail">
                        <img src="img/img-team-1.jpg" class="img-responsive" alt="Image">
                        <figcaption class="caption text-center">
                            <h3>علیرضا محمدی
                                <small>دانشگاه فردوسی مشهد</small>
                            </h3>
                            <ul class="social-links">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-envelope"></i></a></li>
                            </ul>
                        </figcaption>
                    </figure>
                </div>
                <!-- /.col-sm-6 -->

                <div class="col-sm-6 col-md-3">
                    <figure class="thumbnail">
                        <img src="img/img-team-3.jpg" class="img-responsive" alt="Image">
                        <figcaption class="caption text-center">
                            <h3>پویا علیخانی
                                <small>دانشگاه علم و صنعت ایران</small>
                            </h3>
                            <ul class="social-links">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-envelope"></i></a></li>
                            </ul>
                        </figcaption>
                    </figure>
                </div>
                <!-- /.col-sm-6 -->

                <div class="col-sm-6 col-md-3">
                    <figure class="thumbnail">
                        <img src="img/img-team-4.jpg" class="img-responsive" alt="Image">
                        <figcaption class="caption text-center">
                            <h3>سیما مطهری
                                <small>رئیس سازمان صنایع نوین</small>
                            </h3>
                            <ul class="social-links">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-envelope"></i></a></li>
                            </ul>
                        </figcaption>
                    </figure>
                </div>
                <!-- /.col-sm-6 -->

            </div>
            <!-- .row -->
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <figure class="thumbnail">
                        <img src="img/img-team-5.jpg" class="img-responsive" alt="Image">
                        <figcaption class="caption text-center">
                            <h3>نسترن فروتن
                                <small>سازمان ترافیک شهری</small>
                            </h3>
                            <ul class="social-links">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-envelope"></i></a></li>
                            </ul>
                        </figcaption>
                    </figure>
                </div>
                <!-- /.col-sm-6 -->

                <div class="col-sm-6 col-md-3">
                    <figure class="thumbnail">
                        <img src="img/img-team-6.jpg" class="img-responsive" alt="Image">
                        <figcaption class="caption text-center">
                            <h3>لیلا نیکخواه

                            </h3>
                            <ul class="social-links">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-envelope"></i></a></li>
                            </ul>
                        </figcaption>
                    </figure>
                </div>
                <!-- /.col-sm-6 -->

                <div class="col-sm-6 col-md-3">
                    <figure class="thumbnail">
                        <img src="img/img-team-7.jpg" class="img-responsive" alt="Image">
                        <figcaption class="caption text-center">
                            <h3>همایون پورمحمد

                            </h3>
                            <ul class="social-links">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-envelope"></i></a></li>
                            </ul>
                        </figcaption>
                    </figure>
                </div>
                <!-- /.col-sm-6 -->

                <div class="col-sm-6 col-md-3">
                    <figure class="thumbnail">
                        <img src="img/img-team-8.jpg" class="img-responsive" alt="Image">
                        <figcaption class="caption text-center">
                            <h3>صدرا محسنی

                            </h3>
                            <ul class="social-links">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-envelope"></i></a></li>
                            </ul>
                        </figcaption>
                    </figure>
                </div>
                <!-- /.col-sm-6 -->

            </div>
            <!-- .row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- .team -->

    <section id="section-ajenda" class="section-wrapper section-ajenda" style="direction: rtl">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h1>تقویم برگزاری</h1>
                    </div>
                </div>
            </div>

            <div class="row" >
                <div class="col-md-6" >
                    <div class="session">
                        <time>8:30 صبح</time>
                        <h2>باز شدن درب ها</h2>
                    </div>
                    <!-- .session -->

                    <div class="session">
                        <time>9:30 تا 9:45</time>
                        <h2>اهمیت اینترنت اشیا</h2>
                        <h3>لیون گارسیا <span>CO-FOUNDER & CEO, MES</span></h3>
                    </div>


                </div>

                <div class="col-md-6" >

                    <div class="session">
                        <time>9:50 - 10:05AM</time>
                        <h2>Focus on the story: how publishers and brands succeed on today's internet.</h2>
                        <h3>Marie Jung <span>HEAD OF PARTNERSHIPS, MES</span></h3>
                    </div>
                    <!-- .session -->

                    <div class="session">
                        <time>10:10 - 10:35AM</time>
                        <h2>Keynote Presentation</h2>
                        <h3>Zoe Kühn <span>CO-FOUNDER & CEO,, BOOSTER</span></h3>
                    </div>
                </div>


            </div>
        </div>

    </section>


    <section id="section-pricing" class="section-wrapper pricing-section gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h1>شرکت در رویداد</h1>
                        <p>همکنون ثبت نام کنید قبل از اینکه ظرفیت تکمیل شود</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="pricing-table-default text-center">
                        <div class="pricing-head">
                            <h3>ثبت نام زودهنگام-ارائه دهنده</h3>
                            <span class="price">100<sup>هزار تومان</sup></span>
                        </div>
                        <div class="pricing-detail">
                            <ul class="pricing-list pink-i">
                                <li>مخصوص افرادی که مقاله آنها پذیرفته شده است</li>
                                <li>دسترسی به تمام قسمت ها</li>
                                <li>به همراه ناهار در طول رویداد</li>
                            </ul>
                        </div>
                        <a href="#" class="btn btn-primary">ثبت نام</a>
                    </div>
                    <!-- /.pricing-table-wrapper -->
                </div>
                <!-- .col-sm-4 -->

                <div class="col-sm-6">
                    <div class="pricing-table-default text-center">
                        <div class="pricing-head">
                            <h3>ثبت نام زودهنگام-شرکت کننده</h3>
                            <span class="price">200<sup>هزار تومان</sup></span>
                        </div>
                        <div class="pricing-detail">
                            <ul class="pricing-list pink-i">
                                <li>مخصوص افرادی که مقاله آنها پذیرفته شده است</li>
                                <li>دسترسی به تمام پنل ها</li>
                                <li>به همراه ناهار در طول رویداد</li>
                            </ul>
                        </div>
                        <a href="#" class="btn btn-primary">ثبت نام</a>
                    </div>
                    <!-- /.pricing-table-wrapper -->
                </div>
                <!-- .col-sm-4 -->

                <!-- .col-sm-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- .pricing-section -->


    <section id="section-partner" class="section-wrapper client-logo">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h1>PARTNERS</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-sm-4 col-xs-6 section-margin">
                    <a href="#"><img src="img/logo-client-1.jpg" alt="Image"></a>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 section-margin">
                    <a href="#"><img src="img/logo-client-2.jpg" alt="Image"></a>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 section-margin">
                    <a href="#"><img src="img/logo-client-3.jpg" alt="Image"></a>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 section-margin">
                    <a href="#"><img src="img/logo-client-4.jpg" alt="Image"></a>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 section-margin">
                    <a href="#"><img src="img/logo-client-5.jpg" alt="Image"></a>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 section-margin">
                    <a href="#"><img src="img/logo-client-6.jpg" alt="Image"></a>
                </div>
            </div>
        </div>
        <!--end of .container -->
    </section>
    <!-- .client-logo -->


    <section id="section-venue" class="section-wrapper  gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h1>THE VENUE</h1>

                        <p>Credibly recaptiualize cutting-edge functionalities vis-a-vis intuitive growth strategies. Appropriately evolve open-source potentialities via goal-oriented e-business. Enthusiastically target go forward scenarios and visionary "outside the box" thinking. Professionally administrate cost effective sources without corporate convergence.</p>
                    </div>
                </div>
            </div>
            <!-- /.row-->

            <div class="row">
                <div class="col-md-12">
                    <div id="googleMap"></div>
                </div>
            </div>
            <!-- .row-->

            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h2>STAY INFORMED</h2>
                        <p>Don't miss the event!</p>
                    </div>
                    <form id="contactForm" class="subscribe-form form-inline" action="sendemail.php" method="POST">
                        <div class="form-group">
                            <label for="emailTwo" class="sr-only">Email</label>
                            <input type="email" class="form-control input-lg" required="" id="emailTwo" name="email" placeholder="Enter your email address">
                        </div>
                        <button type="submit" class="btn btn-primary btn-lg">SUBSCRIBE</button>
                    </form>
                </div>
            </div>

        </div>
    </section>
    <!-- contact-section -->


    <footer class="footer">
        <div class="copyright-section">
            <div class="container">
                <div class="col-md-12">
                    <div class="copytext text-center">&copy; Conference. All rights reserved | Design By: <a href="https://uicookies.com">uiCookies</a></div>
                </div>
            </div>
            <!-- .container -->
        </div>
        <!-- .copyright-section -->
    </footer>
    <!-- .footer -->

</div>
<!-- #main-wrapper -->


<!-- jquery -->
<script src="js/jquery-2.1.4.min.js"></script>


<!-- Plugin JavaScript -->
<script src="js/jquery.easing.min.js"></script>

<!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/ -->
<script src="https://maps.googleapis.com/maps/api/js"></script>

<!--<script src="js/one-page-nav.js"></script>-->
<script src="js/scripts.js"></script>
</body>
</html>