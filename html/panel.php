<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    //TODO: from @sina to @sina check these comments
    //first we need database access so we require db.php
    require_once 'db.php';
    //we check if there is a session or not,if not start a session
    if (!isset($_SESSION)) {
        session_start();
    }
    //Todo : #ASK what is 'p'?
    if (isset($_GET['p'])) {
        //if 'p' is set to 'logout' then set 'login_user' to isset
        if ($_GET['p'] == 'logout') {
            $_SESSION['login_user'] = '';
        } //if 'p' is set to 'login' then
        else if ($_GET['p'] == 'login') {
            //get email and password and select who have this email and passwoeds
            $query = "SELECT * FROM users WHERE users.email ='" . $_GET['email'] . "' AND users.`password` = '" . $_GET['password'] . "'";
            // if there is such user then
            $res = mysqli_query($connection, $query);
            //TODO: #ASK if not ?
            if (mysqli_num_rows($res) == 1) {
                $_SESSION['login_user'] = $_GET['email']; // Initializing Session
                header("Location: panel.php");
                exit();
            }
        }
    }
    function CheckLogin()
    {
        global $connection;
        //if there is user logged in then
        if (isset($_SESSION['login_user'])) {
            //get details of it
            $query = "SELECT * FROM users WHERE users.email ='" . $_SESSION['login_user'] . "'";
            $res = mysqli_query($connection, $query);
            //if there is a user logged in then
            if (mysqli_num_rows($res) == 1) {
                return true;
            }
        }
        return false;
    }

    function returnPage($page)
    {
        return $_SERVER["PHP_SELF"] . "?p=" . $page;
    }

    //header.php just like a css
    require 'modules/header.php';
    //will check if command is 'update' or 'add' writer will execute them
    //TODO: #ASK why dbcore is needed when it just use when we wanna add or update
    require_once 'modules/dbcore.php';
    ?>
</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header pull-right">
            <a class="navbar-brand" href="#">پنل کاربری اولين همايش شهر هوشمند و اينترنت اشياء</a>
        </div>
        <!--  a navigationBar -->
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav pull-right">
                <li><a href="#">لیست مقالات</a></li>
                <li><a href="#">پنل کاربری</a></li>
                <li class="active" style="margin-right: 100px;"><a href="#">صفحه نخست</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-left">
                <?php
                if(CheckLogin() == false)
                {
                    echo '<li><a href="'.returnPage("loginpage").'"><span></span>ورود</a></li>';
                    echo '<li><a href='.returnPage("register").'><span></span>ثبت نام</a></li>';

                }
                else
                {
                    mysqli_query($connection, "SET NAMES 'utf8'");
                    mysqli_query( $connection,"SET CHARACTER SET 'utf8'");
                    mysqli_query( $connection,"SET character_set_connection = 'utf8'");
                    $query = "SELECT name FROM users WHERE users.email ='" . $_SESSION['login_user'] . "'";
                    $res = mysqli_query($connection, $query);
                    mysqli_data_seek($res, 0);
                    $row = mysqli_fetch_row($res);
                    //TODO: #ASK what is PHP_SELF?
                    echo '<li><a href="' . $_SERVER["PHP_SELF"] . '?p=logout">خروج</a></li>';
                    //Will get name from it
                    echo '<li><a href="#"><span></span>' . $row[0] . '  خوش اومدی </a></li>';
                }
                ?>

            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid text-center" style="min-height: 90%; max-height: 100px;overflow-y: scroll">
    <div class="content">
        <?php
        //will get 'p' and check if p is 'register'
        if (isset($_GET['p'])) {
            if ($_GET['p'] == "register") {
                require 'modules/register.php';
            }
            else if ($_GET['p'] == "showarticle") {
                require 'modules/showarticle.php';
            }//check if logged in
            else if (CheckLogin() == false) {
                require 'modules/login.php';
            }//if not

            else {
                require 'modules/panelmainmenu.php';
            }
        }//if 'p' is not set
        else {
            //if not logged in
            if (CheckLogin() == false) {
                require 'modules/login.php';
            } else {
                if(getUserValue("title") == "دبیر") {
                    require 'modules/executivepanel.php';
                }
                else if(getUserValue("title") == "داور") {
                    require 'modules/judgepanel.php';
                }
                else if(getUserValue("title") == "manager") {
                    require 'modules/managerpanel.php';
                }
                else {
                    require 'modules/panelmainmenu.php';
                }
            }
        }
        ?>
    </div>
</div>

<footer class="container-fluid text-center navbar-fixed-bottom">
    <p>پروژه پایگاه داده حامد جعفرزاده - سینا درویشی</p>
</footer>

</body>
</html>
